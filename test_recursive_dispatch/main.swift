//
//  main.swift
//  test_recursive_dispatch
//
//  Created by François Lamboley on 09/11/2018.
//  Copyright © 2018 happn. All rights reserved.
//

import Foundation

let a = DispatchQueue(label: "a")
let b = DispatchQueue(label: "b")
let c = DispatchQueue(label: "c")
//a.setTarget(queue: c)

dispatch_recursive_sync(a, {
	dispatch_recursive_sync(b, {
		dispatch_recursive_sync(c, {
			dispatch_recursive_sync(a, {
				dispatch_recursive_sync(b, {
					dispatch_recursive_sync(c, {
						dispatch_recursive_sync(a, {
							NSLog("got there")
						})
					})
				})
			})
		})
	})
})
