/*
 * RecursiveDispatch.mm
 * test_recursive_dispatch
 *
 * Created by François Lamboley on 09/11/2018.
 * Copyright © 2018 happn. All rights reserved.
 */

/* From https://stackoverflow.com/a/19838227 */

#include "RecursiveDispatch.h"

#import <unordered_set>
#import <pthread.h>
#import <iostream>



static dispatch_once_t recursiveLockWithDispatchQueueTLSKeyOnceToken;
static pthread_key_t recursiveLockWithDispatchQueueTLSKey;
typedef std::unordered_multiset<const void*> RecursiveLockQueueBag;



static void freeRecursiveLockWithDispatchQueueTLSValue(void* tlsValue) {
	RecursiveLockQueueBag* ms = reinterpret_cast<RecursiveLockQueueBag*>(tlsValue);
	if (ms) delete ms;
}


/* checkAndPushNotPop: If yes, check and push if not on. If no, pop. */
static inline BOOL queueStackCheck(dispatch_queue_t q, BOOL checkAndPushNotPop) {
	dispatch_once(&recursiveLockWithDispatchQueueTLSKeyOnceToken, ^{
		pthread_key_create(&recursiveLockWithDispatchQueueTLSKey, freeRecursiveLockWithDispatchQueueTLSValue);
	});
	
	RecursiveLockQueueBag* ms = reinterpret_cast<RecursiveLockQueueBag*>(pthread_getspecific(recursiveLockWithDispatchQueueTLSKey));
	if (ms == NULL) {
		ms = new RecursiveLockQueueBag();
		pthread_setspecific(recursiveLockWithDispatchQueueTLSKey, reinterpret_cast<const void*>(ms));
	}
	
	const void* const vpq = reinterpret_cast<const void*>((__bridge const void*)q);
	
	BOOL alreadyOn = NO;
	
	if (checkAndPushNotPop) {
		alreadyOn = (ms->count(vpq) > 0);
		if (!alreadyOn) {
			ms->insert(vpq);
		}
	} else {
		ms->erase(vpq);
	}
	return alreadyOn;
}


extern "C" void dispatch_recursive_sync(dispatch_queue_t queue, dispatch_block_t block) {
	if (queueStackCheck(queue, YES)) {
		std::cout << "Direct dispatch" << std::endl;
		block();
	} else {
		@try {
			std::cout << "GCD dispatch" << std::endl;
			dispatch_sync(queue, block);
		} @finally {
			queueStackCheck(queue, NO);
		}
	}
}
